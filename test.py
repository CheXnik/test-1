import csv
import random

from faker import Faker
fake = Faker('ru_RU')

from flask import Flask, request, Response
app = Flask(__name__)

@app.route('/requirements')
def get_requirements():
    f = open('requirements.txt', 'r')
    return f.read()

@app.route('/')
def hello_world():
    return 'Hello World!'

@app.route('/random_users')
def gen_random_users():
    user = list()
    i = 0
    for i in range(100):
        user.append(fake.name())
        user.append(fake.email())
    #print(user)
    return str(user)

@app.route('/avr_data')
def get_avr_data():
    with open('hw.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        counter = 1
        index = random.randint(1, 24000)
        index1 = index
        height_sum = 0
        weight_sum = 0
        print(index)
        for row in reader:
            if counter == index:
                height_sum += float(row[' "Height(Inches)"'])
                weight_sum += float(row[' "Weight(Pounds)"'])
                if index < (index1 + 100):
                    index += 1
            counter += 1
    return f'средний рост={(weight_sum/100)}, средний вес= {(height_sum/100)}'

if __name__ == '__main__':
    app.run(host="127.0.0.1", port="8003")